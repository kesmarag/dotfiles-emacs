(require 'org)
(require 'package)
(setq vc-follow-symlinks t)
(setq package-enable-at-startup nil)
(setq sml/no-confirm-load-theme t)
;;; remove SC if you are not using sunrise commander and org if you like outdated packages

(setq package-archives '(("ELPA"  . "http://tromey.com/elpa/")
                         ("gnu" . "http://elpa.gnu.org/packages/")
			 ("melpa" . "https://melpa.org/packages/")))
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
(package-initialize)

;; Bootstrap `use-package'
(unless
(package-installed-p 'use-package)
(package-refresh-contents)
(package-install 'use-package)
)

(eval-when-compile
(require 'use-package)
)

(org-babel-load-file
(expand-file-name "config.org"
user-emacs-directory))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (elfeed diredfl use-package switch-window rust-mode rainbow-delimiters pyvenv pdf-tools moody modus-vivendi-theme modus-operandi-theme magit keycast highlight-numbers eglot dired-subtree diminish bongo auto-dictionary auctex-latexmk))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
