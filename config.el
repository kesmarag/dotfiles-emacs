(setq user-full-name "Costas Smaragdakis"
      user-mail-address "kesmarag@gmail.com")

(use-package modus-operandi-theme
    :ensure t)


(use-package modus-vivendi-theme
    :ensure t)

  (if (daemonp)
	      (add-hook 'after-make-frame-functions 
			(lambda (frame) 
			(with-selected-frame frame (load-theme 'modus-vivendi t)))) 
	    (load-theme 'modus-vivendi t)
      )

  (defun set-dark-wm-theme (frame)
    (select-frame frame) ;; this is important!
    (when (display-graphic-p)
      (progn
	(when (file-exists-p "/usr/bin/xprop")
      (progn
	(defvar winid nil)
	(setq winid (frame-parameter frame 'outer-window-id))
	(call-process "xprop" nil nil nil "-f" "_GTK_THEME_VARIANT" "8u" "-set" "_GTK_THEME_VARIANT" "dark" "-id" winid))))))

  (defun on-after-init ()
    (set-dark-wm-theme (selected-frame))
    (unless (display-graphic-p (selected-frame))
      (set-face-background 'default "unspecified-bg" (selected-frame))))

  (add-hook 'window-setup-hook 'on-after-init)

  (add-hook 'after-make-frame-functions 'set-dark-wm-theme)

  ;;(set-default-font "IBM Plex Mono-11.5")
  (set-default-font "Hack-12.5")
  ;;(add-to-list 'default-frame-alist '(height . 50))
  ;;(add-to-list 'default-frame-alist '(width . 200))
  (add-to-list 'default-frame-alist '(fullscreen . maximized))
  ;;(add-to-list 'default-frame-alist '(font . "IBM Plex Mono-11.5"))
  (add-to-list 'default-frame-alist '(font . "Hack-12.5"))

   (use-package rainbow-delimiters
       :ensure t)
     (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)

    (use-package highlight-numbers
       :ensure t)
       '(highlight-numbers-number ((t (:foreground "#b48ead"))))  
     (add-hook 'prog-mode-hook 'highlight-numbers-mode)

  ;; remove modeline "lighters"
  (use-package diminish
  :ensure
  :after use-package)

(use-package emacs
  :config
  (setq mode-line-percent-position '(-3 "%p"))
  (setq mode-line-defining-kbd-macro
        (propertize " Macro" 'face 'mode-line-emphasis))
  (setq-default mode-line-format
                '("%e"
                  mode-line-front-space
                  mode-line-mule-info
                  mode-line-client
                  mode-line-modified
                  mode-line-remote
                  mode-line-frame-identification
                  mode-line-buffer-identification
                  "  "
                  mode-line-position
                  (vc-mode vc-mode)
                  " "
                  mode-line-modes
                  " "
                  mode-line-misc-info
                  mode-line-end-spaces)))

(use-package moody :ensure)

(use-package keycast
  :ensure
  :after moody
  :commands keycast-mode
  :config
  (setq keycast-window-predicate 'moody-window-active-p)
  (setq keycast-separator-width 1)
  (setq keycast-insert-after 'mode-line-buffer-identification)
  (setq keycast-remove-tail-elements nil))

(keycast-mode)

(setq inhibit-startup-screen t)
  (toggle-scroll-bar -1)
  (tool-bar-mode -1)
  (menu-bar-mode -1)
  (defun my/disable-scroll-bars (frame)
    (modify-frame-parameters frame
           '((vertical-scroll-bars . nil)
             (horizontal-scroll-bars . nil))))
  (add-hook 'after-make-frame-functions 'my/disable-scroll-bars)
  (setq ring-bell-function 'ignore)
  (setq line-number-mode t)
  (setq column-number-mode t)
  (setq-default initial-scratch-message 
    (concat ";; " (format-time-string "%A %d %B %Y") "\n\n"))

  (global-subword-mode 1)
  (setq make-backup-files nil)
  (setq auto-save-default nil)
  (defalias 'yes-or-no-p 'y-or-n-p)
  (setq global-auto-complete-mode nil)
  (setq tramp-default-method "ssh")
  (show-paren-mode 1)
  (setq mouse-wheel-scroll-amount '(1))
  (global-set-key (kbd "C-`") 'toggle-input-method)

  (defun kesmarag/copy-line()
    (interactive)
    (move-beginning-of-line 1)
    (kill-line)
    (yank)
  )


 ;; (global-set-key (kbd "C-S-w") 'kesmarag/copy-line)

(use-package electric
  :config
  (setq electric-pair-inhibit-predicate'electric-pair-conservative-inhibit)
  (setq electric-pair-preserve-balance t)
  (setq electric-pair-pairs
        '((8216 . 8217)
          (8220 . 8221)
          (171 . 187)))
  (setq electric-pair-skip-self 'electric-pair-default-skip-self)
  (setq electric-pair-skip-whitespace nil)
  (setq electric-pair-skip-whitespace-chars
        '(9
          10
          32))
  (setq electric-quote-context-sensitive t)
  (setq electric-quote-paragraph t)
  (setq electric-quote-string nil)
  (setq electric-quote-replace-double t)
  :hook (after-init-hook . (lambda ()
                             (electric-indent-mode 1)
                             (electric-pair-mode 1)
                             (electric-quote-mode -1))))
(use-package paren
  :config
  (setq show-paren-style 'parenthesis)
  (setq show-paren-when-point-in-periphery t)
  (setq show-paren-when-point-inside-paren nil)
  :hook (after-init-hook . show-paren-mode))

(use-package emacs
  :config
  (setq-default tab-always-indent 'complete)
  (setq-default tab-width 2)
  (setq-default indent-tabs-mode nil))

(setq require-final-newline t)
(defun split-and-follow-horizontally ()
    (interactive)
    (split-window-below)
    (balance-windows)
    (other-window 1))
(global-set-key (kbd "C-x 2") 'split-and-follow-horizontally)
(defun split-and-follow-vertically ()
    (interactive)
    (split-window-right)
    (balance-windows)
    (other-window 1))
(global-set-key (kbd "C-x 3") 'split-and-follow-vertically)

(defun kesmarag/kill-current-buffer ()
    (interactive)
    (kill-buffer (current-buffer)))

(global-set-key (kbd "C-x k") 'kesmarag/kill-current-buffer)

(use-package emacs
    :config
    (setq window-divider-default-right-width 1)
    (setq window-divider-default-bottom-width 1)
    (setq window-divider-default-places 'right-only)
    :hook (after-init-hook . window-divider-mode))

(use-package dired-subtree :ensure t
  :after dired
  :config
  (bind-key "<tab>" #'dired-subtree-toggle dired-mode-map)
  (bind-key "<backtab>" #'dired-subtree-cycle dired-mode-map))
(require 'ls-lisp)
(setq ls-lisp-use-insert-directory-program nil)
(setq ls-lisp-dirs-first t)
(add-hook 'dired-mode-hook
      (lambda ()
        (dired-hide-details-mode)
        (dired-sort-toggle-or-edit)))
(setq dired-listing-switches "-alh")
(require 'dired-x)
(setq-default dired-omit-files-p t) ; Buffer-local variable
(setq dired-omit-files (concat dired-omit-files "\\|^\\..+$"))

(use-package wdired
  :after dired
  :commands wdired-change-to-wdired-mode
  :config
  (setq wdired-allow-to-change-permissions t)
  (setq wdired-create-parent-directories t))

(use-package diredfl
  :ensure
  :hook (dired-mode-hook . diredfl-mode))

(diredfl-global-mode 1)

(use-package eshell
  :bind ("<s-return>" . eshell))

(defun eshell/clear ()
  "Clear the eshell buffer."
  (let ((inhibit-read-only t))
    (erase-buffer)
    (eshell-send-input)))

(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)))

(set-face-attribute 'org-level-1 nil :height 1.25)
(set-face-attribute 'org-level-2 nil :height 1.125)
(set-face-attribute 'org-level-3 nil :height 1.0)
;;(set-face-attribute 'org-table nil :foreground "#b8bb26")
(global-set-key (kbd "C-c o") 
                (lambda () (interactive) (find-file "~/Org/organizer.org")))
(global-set-key (kbd "C-c c") 'org-capture)
(setq org-default-notes-file "~/Org/organizer.org")
   (eval-after-load "ox-latex"
     ;; update the list of LaTeX classes and associated header (encoding, etc.)
     ;; and structure
     '(add-to-list 'org-latex-classes
                   `("beamer"
                     ,(concat "\\documentclass[presentation,12pt]{beamer}\n"
                              "[DEFAULT-PACKAGES]"
                              "[PACKAGES]"
                              "[EXTRA]\n")
                     ("\\section{%s}" . "\\section*{%s}")
                     ("\\subsection{%s}" . "\\subsection*{%s}")
                     ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))
   (setq org-latex-listings t)
   (setq org-pretty-entitles t)
   (setq org-latex-create-formula-image-program 'dvipng)
   (setq org-highlight-latex-and-related '(latex))
   (setq org-format-latex-options (plist-put org-format-latex-options :scale 1.0))

(use-package pyvenv
:ensure t
:init
(setenv "WORKON_HOME" "/home/kesmarag/.conda/envs")
(pyvenv-mode 1)
(pyvenv-tracking-mode 1))
(add-hook 'python-mode-hook (lambda() (pyvenv-workon "keras")))

(use-package rust-mode
 :ensure t)
(require 'rust-mode)



(use-package minibuffer
:config
  (setq completion-category-defaults nil)
  (setq completion-cycle-threshold 3)
  (setq completion-flex-nospace nil)
  (setq completion-pcm-complete-word-inserts-delimiters t)
  (setq completion-pcm-word-delimiters "-_./:| ")
  (setq completion-show-help nil)
  (setq completion-ignore-case t)
  (setq read-buffer-completion-ignore-case t)
  (setq read-file-name-completion-ignore-case t)
  (setq enable-recursive-minibuffers t)
  (file-name-shadow-mode 1)
  (minibuffer-depth-indicate-mode 1)
  (minibuffer-electric-default-mode 1)
  (setq read-answer-short t)
  :bind(
     :map minibuffer-local-completion-map 
     ("<return>" . minibuffer-force-complete-and-exit)
     ("C-j" . exit-minibuffer))
)

(use-package icomplete
    :demand
    :after minibuffer
    :config
    (setq icomplete-delay-completions-threshold 64)
    (setq icomplete-max-delay-chars 2)
    (setq icomplete-show-matches-on-no-input t)
    (setq icomplete-hide-common-prefix nil)
    (setq icomplete-prospects-height 1)
    (setq icomplete-separator (propertize " ┆ " 'face 'shadow))
    (setq icomplete-with-completion-tables t)
    (setq icomplete-in-buffer nil)
    (setq icomplete-tidy-shadowed-file-names nil)

    (icomplete-mode 1)

    ;; :hook (icomplete-minibuffer-setup-hook . prot/icomplete-minibuffer-truncate)
    :bind (:map icomplete-minibuffer-map
    ;; ("<tab>" . icomplete-force-complete)
    ("<return>" . icomplete-force-complete-and-exit)
    ("C-j" . exit-minibuffer)
    ("C-n" . icomplete-forward-completions)
    ("<right>" . icomplete-forward-completions)
    ("<down>" . icomplete-forward-completions)
    ("C-p" . icomplete-backward-completions)
    ("<left>" . icomplete-backward-completions)
    ("<up>" . icomplete-backward-completions)
    )
)

(use-package eglot
  :ensure t)

(defvar kesmarag-default-pyls "/usr/local/anaconda3/bin/pyls")

(setq eglot-put-doc-in-help-buffer nil)
(setq eglot-auto-display-help-buffer nil)
(setq eglot-ignored-server-capabilites '(:documentHighlightProvider))

(add-hook 'python-mode-hook 'eglot-ensure)

(global-set-key (kbd "<f8>")
                (lambda()(interactive)
                  (ispell-change-dictionary "el_GR")
                  (flyspell-buffer)))

(global-set-key (kbd "<f9>")
                (lambda()(interactive)
                  (ispell-change-dictionary "en_US")
                  (flyspell-buffer)))

(use-package pdf-tools
    :ensure t
    :config
    (pdf-tools-install)
    (setq-default pdf-view-display-size 'fit-page)
    (setq pdf-annot-activate-created-annotations t)
    )

(use-package auctex-latexmk
  :ensure t
  :config
  (auctex-latexmk-setup)
  (setq auctex-latexmk-inherit-TeX-PDF-mode t))

(use-package reftex
  :ensure t
  :defer t
  :config
  (setq reftex-cite-prompt-optional-args t)) ;; Prompt for empty optional arguments in cite

(use-package auto-dictionary
  :ensure t
  :init(add-hook 'flyspell-mode-hook (lambda () (auto-dictionary-mode 1))))


(use-package tex
  :ensure auctex
  :mode ("\\.tex\\'" . latex-mode)
  :config (progn
	    (setq TeX-source-correlate-mode t)
	    (setq TeX-source-correlate-method 'synctex)
	    (setq TeX-auto-save t)
	    (setq TeX-parse-self t)
	    ;; (setq-default TeX-master "17th.tex")
	    (setq reftex-plug-into-AUCTeX t)
	    (pdf-tools-install)
	    (setq TeX-view-program-selection '((output-pdf "PDF Tools"))
		  TeX-source-correlate-start-server t)
	    ;; Update PDF buffers after successful LaTeX runs
	    (add-hook 'TeX-after-compilation-finished-functions
		      #'TeX-revert-document-buffer)
	    (add-hook 'LaTeX-mode-hook
		      (lambda ()
			(reftex-mode t)
			(flyspell-mode t)))
	    ))

(use-package bongo
  :ensure
  :commands bongo
  :config
  (setq bongo-default-directory "~/Music")
  (setq bongo-prefer-library-buffers nil)
  (setq bongo-insert-whole-directory-trees t)
  (setq bongo-logo nil)
  (setq bongo-action-track-icon nil)
  (setq bongo-display-track-icons nil)
  (setq bongo-display-track-lengths nil)
  (setq bongo-display-header-icons nil)
  (setq bongo-display-playback-mode-indicator t)
  (setq bongo-display-inline-playback-progress nil)
  (setq bongo-mark-played-tracks nil)
  (setq bongo-header-line-mode nil)
  (setq bongo-header-line-function nil)
  (setq bongo-mode-line-indicator-mode nil)
  (setq bongo-enabled-backends '(vlc))
  (setq bongo-mpv-program-name "mpv")  
  (setq bongo-vlc-program-name "cvlc")
)

;;   (use-package auth-source
;;   :config
;;   (setq auth-sources '("~/.authinfo.gpg" "~/.authinfo"))
;;   (setq user-full-name "Costas Smaragdakis")
;;   (setq user-mail-address "kesmarag@gmail.com"))

;;   (add-hook 'gnus-group-mode-hook 'gnus-topic-mode)


;;   (use-package gnus
;;     :config
;;     (setq gnus-select-method '(nnnil))
;;     (setq gnus-secondary-select-methods
;;         '((nnreddit "")
;;           (nntp "news.gwene.org")
;;           (nnimap "gmail"
;;               (nnimap-address "imap.gmail.com")
;;               (nnimap-stream ssl)
;;               (nnimap-authinfo-file "~/.authinfo.gpg"))
;;           (nnimap "uoc"
;;               (nnimap-address "imap.uoc.gr")
;;               (nnimap-stream ssl)
;;               (nnimap-authinfo-file "~/.authinfo.gpg"))
;;           (nnimap "iacm"
;;               (nnimap-address "mail.iacm.forth.gr")
;;               (nnimap-stream ssl)
;;               (nnimap-authinfo-file "~/.authinfo.gpg"))   
;;            ))
;;     (setq gnus-gcc-mark-as-read t)
;;     (setq gnus-novice-user nil)
;;     :bind ("C-c m" . gnus))

;;   (use-package gnus-async
;;     :after gnus
;;     :config
;;     (setq gnus-asynchronous t)
;;     (setq gnus-use-article-prefetch 15))

;;   (use-package gnus-group
;;     :after gnus
;;     :demand
;;     :config
;;     (setq gnus-level-subscribed 6)
;;     (setq gnus-level-unsubscribed 7)
;;     (setq gnus-level-zombie 8)
;;     (setq gnus-activate-level 4)
;;     (setq gnus-list-groups-with-ticked-articles nil)
;;     (setq gnus-group-sort-function
;;           '((gnus-group-sort-by-unread)
;;             (gnus-group-sort-by-alphabet)
;;             (gnus-group-sort-by-rank)))
;;     (setq gnus-group-line-format "%M%p%P%5y:%B%(%c%)\n")
;;     (setq gnus-group-mode-line-format "%%b")
;;     :hook ((gnus-group-mode-hook . hl-line-mode)
;;            (gnus-select-group-hook . gnus-group-set-timestamp)))

;;   (use-package gnus-topic
;;     :after (gnus gnus-group)
;;     :config
;;     (setq gnus-topic-display-empty-topics nil)
;;     :hook (gnus-group-mode-hook . gnus-topic-mode))

;;   (use-package gnus-sum
;;     :after (gnus gnus-group)
;;     :demand
;;     :config
;;     (setq gnus-auto-select-first nil)
;;     (setq gnus-summary-ignore-duplicates t)
;;     (setq gnus-suppress-duplicates t)
;;     (setq gnus-save-duplicate-list t)
;;     (setq gnus-summary-goto-unread nil)
;;     (setq gnus-summary-make-false-root 'adopt)
;;     (setq gnus-summary-thread-gathering-function
;;           'gnus-gather-threads-by-subject)
;;     (setq gnus-thread-sort-functions
;;           '((not gnus-thread-sort-by-date)
;;             (not gnus-thread-sort-by-number)))
;;     (setq gnus-subthread-sort-functions
;;           'gnus-thread-sort-by-date)
;;     (setq gnus-thread-hide-subtree nil)
;;     (setq gnus-thread-ignore-subject nil)
;;     (setq gnus-user-date-format-alist
;;           '(((gnus-seconds-today) . "Today at %R")
;;             ((+ 86400 (gnus-seconds-today)) . "Yesterday, %R")
;;             (t . "%Y-%m-%d %R")))
;;     (setq gnus-ignored-from-addresses "Costas Smaragdakis")
;;     (setq gnus-summary-to-prefix "To: ")

;;     (setq gnus-summary-line-format "%U%R%z %-16,16&user-date;  %4L:%-30,30f  %B%s\n")
;;     (setq gnus-summary-mode-line-format "%p")
;;     (setq gnus-sum-thread-tree-false-root "─┬> ")
;;     (setq gnus-sum-thread-tree-indent " ")
;;     (setq gnus-sum-thread-tree-leaf-with-other "├─> ")
;;     (setq gnus-sum-thread-tree-root "")
;;     (setq gnus-sum-thread-tree-single-leaf "└─> ")
;;     (setq gnus-sum-thread-tree-vertical "│")
;;     :hook (gnus-summary-mode-hook . hl-line-mode)
;; )

;; (setq message-sendmail-f-is-evil 't)
;; (setq message-send-mail-function 'message-send-mail-with-sendmail)
;; (setq sendmail-program "/usr/bin/msmtp")

;; (defun cg-feed-msmtp ()
;;   (if (message-mail-p)
;;   (save-excursion
;;   (let* ((from
;;   (save-restriction
;;   (message-narrow-to-headers)
;;   (message-fetch-field "from")))
;;   (account
;;   (cond
;;   ;; I use email address as account label in ~/.msmtprc
;;   ((string-match "kesmarag@uoc.gr" from)"kesmarag@uoc.gr")
;;   ;; Add more string-match lines for your email accounts
;;   ((string-match "kesmarag@gmail.com" from)"kesmarag@gmail.com")
;;   ((string-match "kesmarag@iacm.forth.gr" from)"kesmarag@iacm.forth.gr"))))
;;   (setq message-sendmail-extra-arguments (list '"-a" account)))))) 

;; (setq message-sendmail-envelope-from 'header)
;; (add-hook 'message-send-mail-hook 'cg-feed-msmtp)


;; (use-package ebdb
;;   :ensure
;;   :pin gnu                              ; Prefer ELPA version
;;   :config
;;   (require 'ebdb-gnus)
;;   (require 'ebdb-message)
;;   (setq ebdb-default-window-size 0.2)
;;   (setq ebdb-mua-pop-up nil)
;;   (setq ebdb-mua-auto-update-p 'query)
;;   (setq ebdb-mua-sender-update-p 'query)
;;   (setq ebdb-mua-reader-update-p 'query)
;;   (setq ebdb-add-aka 'query)
;;   (setq ebdb-add-name 'query)
;;   (setq ebdb-add-mails 'query))

;; (use-package gnus-art
;;   :after gnus
;;   :demand
;;   :config
;;   (setq gnus-article-browse-delete-temp 'ask)
;;   (setq gnus-article-over-scroll nil)
;;   (setq gnus-article-show-cursor t)
;;   (setq gnus-article-sort-functions
;;         '((not gnus-article-sort-by-number)
;;           (not gnus-article-sort-by-date)))
;;   (setq gnus-article-truncate-lines nil)
;;   (setq gnus-html-frame-width 80)
;;   (setq gnus-html-image-automatic-caching t)
;;   (setq gnus-inhibit-images t)
;;   (setq gnus-max-image-proportion 0.7)
;;   (setq gnus-treat-display-smileys nil)
;;   (setq gnus-article-mode-line-format "%G %S %m")
;;   (setq gnus-visible-headers
;;         '("^From:" "^To:" "^Cc:" "^Newsgroups:" "^Subject:" "^Date:"
;;           "Followup-To:" "Reply-To:" "^Organization:" "^X-Newsreader:"
;;           "^X-Mailer:"))
;;   (setq gnus-sorted-header-list gnus-visible-headers)
;;   :bind (:map gnus-article-mode-map
;;               ("i" . gnus-article-show-images)
;;               ("s" . gnus-mime-save-part)
;;               ("o" . gnus-mime-copy-part)))

;; (use-package gnus-srvr
;;   :after gnus
;;   :hook ((gnus-browse-mode-hook gnus-server-mode-hook) . hl-line-mode))



;; (use-package gnus-dired
;;   :after (gnus dired)
;;   :hook (dired-mode-hook . gnus-dired-mode))

; use msmtp
  (setq message-send-mail-function 'message-send-mail-with-sendmail)
  (setq sendmail-program "/usr/bin/msmtp")
  ; tell msmtp to choose the SMTP server according to the from field in the outgoing email
  (setq message-sendmail-extra-arguments '("--read-envelope-from"))
  (setq message-sendmail-f-is-evil 't)


    (require 'mu4e)
    (eval-after-load 'mu4e-utils
    '(defun mu4e-info-handler (info)))

    (setq mu4e-maildir (expand-file-name "~/.gmail"))

    (setq mu4e-drafts-folder "/Drafts")
    (setq mu4e-sent-folder   "/Sent_Items")
    (setq mu4e-trash-folder  "/Trash")
    (setq message-signature-file "~/.signature") 

    ; get mail
    (setq mu4e-get-mail-command "mbsync -c ~/.emacs.d/.mbsyncrc gmail"
	  ;; mu4e-html2text-command "/usr/bin/w3m -T text/html"
	  mu4e-update-interval 600
	  mu4e-headers-auto-update t
	  mu4e-compose-signature-auto-include t)

    ;; From Ben Maughan: Get some Org functionality in compose buffer
    (add-hook 'message-mode-hook 'turn-on-orgtbl)
    (add-hook 'message-mode-hook 'turn-on-orgstruct++)

    (setq mu4e-maildir-shortcuts
    '( ("/INBOX"        . ?i)
       ("/Sent_Items"   . ?s)
       ("/Trash"        . ?t)
       ("/Drafts"       . ?d)))

    ;; the list of all of my e-mail addresses
    (setq mu4e-user-mail-address-list '("kesmarag@gmail.com"
					"kesmarag@uoc.gr"))

    ;; show images
    (setq mu4e-show-images t)

    ;; use imagemagick, if available
    (when (fboundp 'imagemagick-register-types)
      (imagemagick-register-types))

    ;; general emacs mail settings; used when composing e-mail
    ;; the non-mu4e-* stuff is inherited from emacs/message-mode
    (setq mu4e-reply-to-address "kesmarag@gmail.com"
	user-mail-address "kesmarag@gmail.com"
	user-full-name  "Costas Smaragdakis")

    ;; don't save message to Sent Messages, IMAP takes care of this
    (setq mu4e-sent-messages-behavior 'delete)

    ;; spell check
     (add-hook 'mu4e-compose-mode-hook
	  (defun my-do-compose-stuff ()
	     "My settings for message composition."
	     (set-fill-column 72)
	     (flyspell-mode)))


  (defun bms/mu4e-sometimes-silent-update-force ()
    "Only show minibuffer information when explicitly called."
    (interactive)
    (setq mu4e-hide-index-messages nil)
    (mu4e-maildirs-extension-force-update)
    (sleep-for 5)
    (setq mu4e-hide-index-messages t))

  (defun bms/mu4e-front-keys ()
    "For use on mu4e main menu screen."
    (local-set-key (kbd "u") 'bms/mu4e-sometimes-silent-update-force))

  (add-hook 'mu4e-main-mode-hook 'bms/mu4e-front-keys) 


(require 'mu4e-contrib)
(setq mu4e-html2text-command 'mu4e-shr2text)
;;(setq mu4e-html2text-command "iconv -c -t utf-8 | pandoc -f html -t plain")
(add-to-list 'mu4e-view-actions '("ViewInBrowser" . mu4e-action-view-in-browser) t)

(setq mu4e-view-html-plaintext-ratio-heuristic  most-positive-fixnum)


;;  (setq mu4e-view-prefer-html t)

(use-package elfeed
  :ensure
  :config
  (setq elfeed-use-curl t)
  (setq elfeed-curl-max-connections 10)
  (setq elfeed-db-directory "~/.emacs.d/elfeed/")
  (setq elfeed-enclosure-default-dir "~/Downloads/")
  (setq elfeed-search-filter "@4-months-ago +unread")
  (setq elfeed-sort-order 'descending)
  (setq elfeed-search-clipboard-type 'CLIPBOARD)
  (setq elfeed-search-title-max-width 100)
  (setq elfeed-search-title-min-width 30)
  (setq elfeed-search-trailing-width 25)
  (setq elfeed-show-truncate-long-urls t)
  (setq elfeed-show-unique-buffers t)
)

(setq elfeed-feeds
      '(("https://planet.emacslife.com/atom.xml" emacs community)))
